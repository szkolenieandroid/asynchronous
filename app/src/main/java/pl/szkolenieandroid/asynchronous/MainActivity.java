package pl.szkolenieandroid.asynchronous;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.math.BigInteger;


public class MainActivity extends Activity {

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.result);
    }

    public void start(View view) {
        textView.setText("Calculating...");
        new PrimesTask(this, textView).execute(5000);
    }

    private class PrimesTask extends AsyncTask<Integer, Integer, BigInteger> {

        private Context context;
        private TextView result;
        private ProgressDialog progressDialog;

        private PrimesTask(Context context, TextView result) {
            this.context = context;
            this.result = result;
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("Calculating prime number...");
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setMax(100);
            progressDialog.setProgress(0);
            progressDialog.setCancelable(true);
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    PrimesTask.this.cancel(false);
                }
            });
            progressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            Integer value = values[0];
            progressDialog.setProgress(value);
        }

        @Override
        protected BigInteger doInBackground(Integer... params) {
            int n = params[0];
            BigInteger prime = new BigInteger("2");
            int progress = 0;
            for (int i = 0; i < n && !isCancelled(); i++) {
                int percent = (int) (100f * i) / n;
                if (percent > progress) {
                    publishProgress(percent);
                    progress = percent;
                }
                prime = prime.nextProbablePrime();
            }
            return prime;
        }

        @Override
        protected void onCancelled(BigInteger result) {
            this.result.setText("Canceled");
        }

        @Override
        protected void onPostExecute(BigInteger bigInteger) {
            result.setText(bigInteger.toString());
            progressDialog.dismiss();
        }
    }
}
